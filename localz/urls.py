"""localz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from core.views import *
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
]
urlpatterns += i18n_patterns(
    path(r'', home, name='home'),
    path(r'about-us/', AboutUsView.as_view(), name='about-us'),
    path(r'solutions/', SolutionsView.as_view(),name='solutions'),
    path(r'services/', ServicesView.as_view(),name='services'),
    path(r'contact-us/', ContactUsView.as_view(),name='contact-us'),
    path(r'dev-ops/', DevopsView.as_view(),name='dev-ops'),
    path(r'epa/', EAPView.as_view(),name='epa'),
    path(r'erp/', ERPView.as_view(),name='erp'),
    path(r'microsoft/', MicrosoftSolutionView.as_view(),name='microsoft'),
    path(r'mobile-app/', MobileAppView.as_view(),name='mobile-app'),
    path(r'rpa/', RPAView.as_view(),name='rpa'),
    path(r'why-us/', WhyUSView.as_view(), name='why-us'),
    path(r'application-managment/', AppMangView.as_view(), name='app-mang'),
    path(r'db-managment/', DBManagementView.as_view(), name='db-mang'),
    path(r'e-com/', EComDigiView.as_view(), name='e-com'),
    path(r'productivity/', ProductivityView.as_view(), name='productivity'),

    ######
    path(r'my-it/', MYITView.as_view(), name='myit'),
    path(r'cdc/', DataCenterView.as_view(), name='cdc'),
    path(r'it-spnd/', ITSpendView.as_view(), name='it-spnd'),
    path(r'mtc/', MigToCloudView.as_view(), name='mtc'),
    path(r'mod-app/', ModAppView.as_view(), name='mod-app'),
    path(r'protect-data/', PortectDataView.as_view(), name='protect'),
    path(r'scale-business/', ScalingBusinessView.as_view(), name='scale-business'),

    ###
    path(r'ubantu/', UbantuCloudView.as_view(), name='ubantu'),
    path(r'chef-automat/', ChefAutomatView.as_view(), name='chef-automat'),
    path(r'databricks/', DatabricksView.as_view(), name='databricks'),
    path(r'bpm/', BPMView.as_view(), name='bpm'),
    path(r'data-service/', DataServiceView.as_view(), name='data-service'),
    path(r'data-integration/', DataIntegrationView.as_view(), name='data-integration'),
    path(r'mdm/', MDMView.as_view(), name='mdm'),
    path(r'cdsw/', CDSWView.as_view(), name='cdsw'),
    path(r'hdp/', HDPWView.as_view(), name='hdp'),
    path(r'cc/', CCView.as_view(), name='cc'),
    path(r'cos/', COSView.as_view(), name='cos'),
    path(r'document-managment/', DocumentManagmentView.as_view(), name='document-managment'),

    path(r'managed-security/', ManagedSecurityView.as_view(), name='managed-security'),
    path(r'oss/', OpenSourceSecurityView.as_view(), name='oss'),

    path(r'chatbot-view/', ChatbotView.as_view(), name='chatbot-view'),
    path(r'ars/', AugmentedRealityServicesView.as_view(), name='ars'),



) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
