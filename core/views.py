from django.shortcuts import render
from django.views.generic.base import TemplateView


# Create your views here.

def home(request):
    return render(request, "ltr/index1.html")

class AboutUsView(TemplateView):
    template_name = 'ltr/about-us.html'

class SolutionsView(TemplateView):
    template_name = 'ltr/solutions.html'

class ServicesView(TemplateView):
    template_name = 'ltr/why-us.html'

class ContactUsView(TemplateView):
    template_name = 'ltr/contact-us.html'


class DevopsView(TemplateView):
    template_name = 'ltr/devops.html'

class EAPView(TemplateView):
    template_name = 'ltr/enterprise-assurance-program.html'

class ERPView(TemplateView):
    template_name = 'ltr/erp.html'

class MicrosoftSolutionView(TemplateView):
    template_name = 'ltr/microsoft-solutions.html'

class MobileAppView(TemplateView):
    template_name = 'ltr/mobile-app-dev.html'

class RPAView(TemplateView):
    template_name = 'ltr/rpa.html'


class WhyUSView(TemplateView):
    template_name = 'ltr/why-us.html'

class AppMangView(TemplateView):
    template_name = 'ltr/application-managment.html'

class DBManagementView(TemplateView):
    template_name = 'ltr/database-managent.html'

class EComDigiView(TemplateView):
    template_name = 'ltr/ecom-digi.html'

class ProductivityView(TemplateView):
    template_name = 'ltr/productivity.html'

######
class MYITView(TemplateView):
    template_name = 'transfrom-it.html'

class DataCenterView(TemplateView):
    template_name = 'ltr/consider_data_center.html'

class ITSpendView(TemplateView):
    template_name = 'ltr/control_IT_spend.html'


class MigToCloudView(TemplateView):
    template_name = 'ltr/migration_to_cloud.html'


class ModAppView(TemplateView):
    template_name = 'ltr/moderization-app.html'


class PortectDataView(TemplateView):
    template_name = 'ltr/protecting-data.html'

class ScalingBusinessView(TemplateView):
    template_name = 'ltr/scaling-my-business.html'



#
class UbantuCloudView(TemplateView):
    template_name = 'ltr/why-us.html'

class ChefAutomatView(TemplateView):
    template_name = 'ltr/why-us.html'

class DatabricksView(TemplateView):
    template_name = 'ltr/why-us.html'

class BPMView(TemplateView):
    template_name = 'ltr/why-us.html'

class DataServiceView(TemplateView):
    template_name = 'ltr/why-us.html'

class DataIntegrationView(TemplateView):
    template_name = 'ltr/why-us.html'

class MDMView(TemplateView):
    template_name = 'ltr/why-us.html'

class CDSWView(TemplateView):
    template_name = 'ltr/why-us.html'

class HDPWView(TemplateView):
    template_name = 'ltr/why-us.html'

class CCView(TemplateView):
    template_name = 'ltr/why-us.html'

class COSView(TemplateView):
    template_name = 'ltr/why-us.html'

class DocumentManagmentView(TemplateView):
    template_name = 'ltr/why-us.html'


class ManagedSecurityView(TemplateView):
    template_name = 'ltr/why-us.html'


class OpenSourceSecurityView(TemplateView):
    template_name = 'ltr/why-us.html'

class ChatbotView(TemplateView):
    template_name = 'ltr/why-us.html'

class AugmentedRealityServicesView(TemplateView):
    template_name = 'ltr/why-us.html'